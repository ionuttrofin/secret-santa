<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect()->route('error');
});

/*
 * Routes for preview emails on browser
 */
//Route::get('/preview-notification', function () {
//
//    $message = (new \App\Notifications\WhoWannabeSanta())->toMail('ionut.trofin@grapefruit.ro');
//
//    $markdown = new \Illuminate\Mail\Markdown(view(), config('mail.markdown'));
//    return $markdown->render('vendor.notifications.email', $message->toArray());
//});
//
//Route::get('/preview-notification-santa', function () {
//
//    $message = (new \App\Notifications\NotifySanta(\App\Grapefruiteer::first()))->toMail('ionut.trofin@grapefruit.ro');
//
//    $markdown = new \Illuminate\Mail\Markdown(view(), config('mail.markdown'));
//    return $markdown->render('vendor.notifications.email', $message->data());
//});

Route::get('/import', 'SantaController@import')->name('get.import');
Route::post('/import', 'SantaController@importUsers')->name('post.import');

Route::get('/wannabe-santa/{id}', 'SantaController@index')->name('index');
Route::post('/wannabe-santa', 'SantaController@store')->name('pick');

Route::get('/good-santa', 'SantaController@success')->name('success');
Route::get('/bad-santa', 'SantaController@error')->name('error');

Route::get('/notifyGrapefruiteers', 'SantaController@notifyGrapefruiteers');


Route::group(['prefix' => 'xmas2019', 'as' => 'xmas2019.'], function(){
    Route::get('/import', 'XmasController@import')->name('get.import');
    Route::post('/import', 'XmasController@importUsers')->name('post.import');

    Route::get('/wannabe-santa/{id}', 'XmasController@index')->name('index');
    Route::post('/wannabe-santa', 'XmasController@store')->name('pick');

    Route::get('/good-santa', 'XmasController@success')->name('success');
    Route::get('/bad-santa', 'XmasController@error')->name('error');

    Route::get('/notifySantas', 'XmasController@notifySantas');
});