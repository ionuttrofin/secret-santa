<?php

use Illuminate\Database\Seeder;

class GrapefruiteersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i <= 50; $i++) {
            DB::table('grapefruiteers')->insert([
                'name' => str_random(10),
                'email' => 'ionut.trofin@grapefruit.ro',
            ]);
        }
    }
}
