@extends('xmas2019.layout')

@section('content')
    <!--  Step 1  -->
    <div class="window">
        <div class="santa">
            <div class="head">
                <div class="face">
                    <div class="redhat">
                        <div class="whitepart"></div>
                        <div class="redpart"></div>
                        <div class="hatball"></div>
                    </div>
                    <div class="eyes"></div>
                    <div class="beard">
                        <div class="nouse"></div>
                        <div class="mouth"></div>
                    </div>
                </div>
                <div class="ears"></div>
            </div>
            <div class="body"></div>
        </div>
    </div>

    <div class="message">
        <h1>Secret Santa!</h1>
        <h1>{{ $wannabeSanta->first_name . ' ' . $wannabeSanta->last_name }}</h1>


        <h2 class="copyright">Vrei sa aduci bucurie?</h2>
{{--        <h2 class="copyright">Cine vrea să fie Moș Crăciun în locul meu?</h2>--}}
        <form method="post" action="{{ route('xmas2019.pick') }}">
            {{ csrf_field() }}
{{--            <select name="santa" class="select">--}}
{{--                <option value="" disabled selected>Cine vrea să fie Moș Crăciun în locul meu?</option>--}}
{{--                @foreach($wannabeSanta as $santa)--}}
{{--                    <option value="{{ $santa->id }}">{{ $santa->first_name . ' ' . $santa->last_name }}</option>--}}
{{--                @endforeach--}}
{{--            </select>--}}

            <input type="hidden" name="santa" class="select" value="{{ $wannabeSanta->hash }}">
            <button type="submit" class="button">Vreau</button>
        </form>
    </div>
@endsection()
