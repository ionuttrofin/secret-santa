<table class="" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <h2>{{ $text }}</h2>
            <h1>{{ $slot }}</h1>
        </td>
    </tr>
</table>
