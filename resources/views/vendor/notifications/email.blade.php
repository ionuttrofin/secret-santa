@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level == 'error')
# Whoops!
@else
# Hello!
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
            $color = 'green';
            break;
        case 'error':
            $color = 'red';
            break;
        case 'grapefruit':
            $color = 'pink';
            break;
        default:
            $color = 'blue';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach


@if(! empty ($toWhom))
@component('mail::promotion')
Tu cadou trebui să-i faci lui: <b>{{ $toWhom }}</b><br>
Adresă: {{ $address }}<br>
Telefon: {{ $phone }}
@endcomponent
@endif

@component('mail::bye', ['text' => 'Cu drag,'])
Moș Nicolae
@endcomponent

{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy')
Dacă întâmpini probleme făcând clic pe butonul "{{ $actionText }}", copiază și inserează următoarea adresaâ URL
în browser-ul tău web: [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endisset
@endcomponent
