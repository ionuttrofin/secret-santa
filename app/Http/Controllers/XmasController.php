<?php

namespace App\Http\Controllers;

use App\XmasSanta;
use App\Notifications\XmasNotifySanta;
use App\Notifications\XmasWannabeSanta;
use App\Services\Importer;
use Illuminate\Http\Request;

class XmasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $wannabeSanta = XmasSanta::notSanta()->where('hash', $id)->first();

        if (is_null($wannabeSanta)) {
            return redirect()->route('xmas2019.error');
        }

        return view('xmas2019.index', compact('wannabeSanta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $wannabeSanta = XmasSanta::notSanta()->where('hash', $request->get('santa'))->first();

        if (is_null($wannabeSanta)) {
            return redirect()->route('xmas2019.error');
        }
        $theChosenOne = XmasSanta::notPicked($wannabeSanta->id)->get()->random();

        $wannabeSanta->update([
            'is_santa' => 1
        ]);
        $theChosenOne->update([
            'has_been_picked' => 1
        ]);

        $wannabeSanta->notify(new XmasNotifySanta($theChosenOne));

        return redirect()->route('xmas2019.success')->with('theChosenOne', $theChosenOne);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function success()
    {
        return view('xmas2019.success');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function error()
    {
        return view('xmas2019.error');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function import()
    {
        $santas = XmasSanta::all();

        return view('xmas2019.import', compact('santas'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function importUsers(Request $request)
    {
        $file = $request->file('base');
        $worksheet = $this->storeExcelFile($file);
        $numberOfUsers = $worksheet->getHighestRow();

        for ($row = 2; $row <= $numberOfUsers; ++$row) {
            $first_name = trim($worksheet->getCell('A' . $row)->getCalculatedValue());
            $last_name = trim($worksheet->getCell('B' . $row)->getCalculatedValue());
            $email = trim($worksheet->getCell('C' . $row)->getCalculatedValue());

            $user = XmasSanta::where('email', $email)->first();

            if (is_null($user)) {
                $user = XmasSanta::create([
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'hash' => hash('sha256', $email)
                ]);
            }
        }

        return redirect()->route('xmas2019.get.import');
    }

    /**
     * @param $file
     * @return mixed
     */
    protected function storeExcelFile($file)
    {
        $importFile = Importer::fileToTemp($file);
        $excel = Importer::getExcel($importFile);
        $worksheet = Importer::getSpreadSheet($excel, 'santa');
        return $worksheet;
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function notifySantas()
    {
        $santas = XmasSanta::all();

        foreach ($santas as $grapefruiteer) {
            $grapefruiteer->notify(new XmasWannabeSanta());
        }

        return redirect()->route('xmas2019.get.import');
    }
}
