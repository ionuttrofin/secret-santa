<?php

namespace App\Http\Controllers;

use App\Grapefruiteer;
use App\Services\Importer;

use App\Notifications\NotifySanta;
use App\Notifications\WhoWannabeSanta;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;

class SantaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @return array|Factory|Application|RedirectResponse|View
     */
    public function index($id)
    {
        $wannabeSanta = Grapefruiteer::notSanta()->where('hash', $id)->first();

        if (is_null($wannabeSanta)) {
            return redirect()->route('error');
        }

        return view('index', compact('wannabeSanta'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $wannabeSanta = Grapefruiteer::notSanta()->where('hash', $request->get('santa'))->first();

        if (is_null($wannabeSanta)) {
            return redirect()->route('error');
        }

        $theChosenOne = Grapefruiteer::notPicked($wannabeSanta->id)
            ->inRandomOrder()
            ->first();

        $theChosenOne->update(['has_been_picked' => 1]);

        $wannabeSanta->update(['is_santa' => 1, 'check' => $theChosenOne->id]);
        $wannabeSanta->notify(new NotifySanta($theChosenOne));

        return redirect()->route('success')->with('theChosenOne', $theChosenOne);
    }


    /**
     * @return Factory|View
     */
    public function success()
    {
        return view('success');
    }

    /**
     * @return Factory|View
     */
    public function error()
    {
        return view('error');
    }

    /**
     * @return Factory|View
     */
    public function import()
    {
        $grapefruiteers = Grapefruiteer::all();

        return view('import', compact('grapefruiteers'));
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function importUsers(Request $request)
    {
        $file = $request->file('base');
        $worksheet = $this->storeExcelFile($file);
        $numberOfUsers = $worksheet->getHighestRow();

        for ($row = 2; $row <= $numberOfUsers; ++$row) {
            $first_name = trim($worksheet->getCell('A' . $row)->getCalculatedValue());
            $last_name = trim($worksheet->getCell('B' . $row)->getCalculatedValue());
            $email = trim($worksheet->getCell('C' . $row)->getCalculatedValue());
            $city = trim($worksheet->getCell('D' . $row)->getCalculatedValue());
            $address = trim($worksheet->getCell('E' . $row)->getCalculatedValue());
            $phone = trim($worksheet->getCell('F' . $row)->getCalculatedValue());

            $user = Grapefruiteer::where('email', $email)->first();

            if (is_null($user)) {
                $user = Grapefruiteer::create([
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'phone' => $phone,
                    'address' => $city . ', '. $address,
                    'hash' => hash('sha256', $email)
                ]);
            }
        }

        return redirect()->route('get.import');
    }

    /**
     * @param $file
     * @return mixed
     */
    protected function storeExcelFile($file)
    {
        $importFile = Importer::fileToTemp($file);
        $excel = Importer::getExcel($importFile);
        $worksheet = Importer::getSpreadSheet($excel, 'grapefruiteers');
        return $worksheet;
    }

    /**
     * @return RedirectResponse
     */
    public function notifyGrapefruiteers()
    {
        $grapefruiteers = Grapefruiteer::where('is_santa', 0)->get();

        foreach ($grapefruiteers as $grapefruiteer) {
            $grapefruiteer->notify(new WhoWannabeSanta());
        }

        return redirect()->route('get.import');
    }
}
