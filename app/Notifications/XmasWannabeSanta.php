<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class XmasWannabeSanta extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->from('noreplay@moscraciun.ro', 'Mos Craciun')
            ->subject('Ho Ho Ho')
            ->greeting('Draga ' . $notifiable->first_name)
            ->line('Am inteles ca si anul acesta vrei sa aduci bucurie, apasa pe butonul de mai jos si confirma ca vrei sa fii Mos Craciun.')
            ->action('Vreau sa fiu Mos Craciun', route('xmas2019.index', $notifiable->hash))
            ->salutation('Mos Craciun');


    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
