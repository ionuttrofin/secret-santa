<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class XmasNotifySanta extends Notification
{
    use Queueable;
    /**
     * @var
     */
    private $theChosenOne;

    /**
     * Create a new notification instance.
     *
     * @param $theChosenOne
     */
    public function __construct($theChosenOne)
    {
        //
        $this->theChosenOne = $theChosenOne;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->from('noreply@moscraciun.ro', 'Mos Craciun')
            ->subject('Ho Ho Ho')
            ->greeting('Draga ' . $notifiable->first_name)
            ->line('Iarna-i grea, omatu-i mare / Si ne dardaie dintii cam tare')
            ->line('La alegeri noi am votat / Si de Dancila am scapat')
            ->line('Cum tot raul e spre bine / Un cadou la tine vine')
            ->line('De la un prieten misterios / Ce-ti va lua ceva bengos!')
            ->line('Dar stai fara griji, ca nu ai scapat / La randul tau te duci la cumparat')
            ->line('Ai grija sa te informezi / Sa te si documentezi')
            ->line('Din suflet ceva trebuie sa oferi / Din inima trebuie sa speri')
            ->line('Ca sarbatori fericite toti vom avea / Si cu familia dar si prietenii vei sta.')
            ->line('Tu cadou sa-i faci lui: ' . $this->theChosenOne->first_name . ' ' . $this->theChosenOne->last_name)

            ->salutation('Mos Craciun');


    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
