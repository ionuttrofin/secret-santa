<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class WhoWannabeSanta extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**replyTo
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->priority(1)
            ->level('grapefruit')
            ->from('mosnicolae@grapefruit.ro', 'Moș Nicolae')
            ->replyTo('moscraciun@grapefruit.ro', 'Moș Nicolae')
            ->subject('Adu bucurie')
            ->greeting('Dragă ' . $notifiable->last_name)
            ->line('Am înțeles că și anul acesta vrei să aduci bucurie, apasă pe butonul de mai jos și confirmă că vrei să fii Moș Nicolae.')
            ->action('Vreau să fiu Moș Nicolae', route('index', $notifiable->hash));

    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
