<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class NotifySanta extends Notification
{
    use Queueable;

    /**
     * @var
     */
    private $theChosenOne;

    /**
     * Create a new notification instance.
     *
     * @param $theChosenOne
     */
    public function __construct($theChosenOne)
    {
        //
        $this->theChosenOne = $theChosenOne;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $message = (new MailMessage)
            ->priority(1)
            ->level('grapefruit')
            ->from('mosnicolae@grapefruit.ro', 'Moș Nicolae')
            ->replyTo('moscraciun@grapefruit.ro', 'Moș Nicolae')
            ->subject('Un cadou la tine vine')
            ->greeting('Dragă ' . $notifiable->last_name)
            ->line('Un cadou la tine vine')
            ->line('De la un coleg misterios / Ce-ți va lua ceva bengos!')
            ->line('Dar stai fără griji, că n-ai scăpat / La randul tău te duci la cumpărat')
            ->line('Ai grija să te informezi / Să te și documentezi')
            ->line('Fără calendare, șosete sau alte acarete / Fără dulciuri, chiloți sau trompete')
            ->line('Din suflet ceva trebuie să oferi / Din inimă trebuie să speri')
            ->line('Ca sărbători fericite toți vom avea / Și cu familia în casă vei sta.');

        $viewData = [
            'toWhom' => $this->theChosenOne->first_name . ' ' . $this->theChosenOne->last_name,
            'address' => $this->theChosenOne->address,
            'phone' => $this->theChosenOne->phone
        ];
        $message->markdown('vendor.notifications.email', $viewData);

        return $message;

    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
