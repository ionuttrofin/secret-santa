<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class XmasSanta extends Model
{
    use Notifiable;

    protected $table = 'santas';

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'is_santa',
        'has_been_picked',
        'hash',
    ];

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotSanta($query)
    {
        return $query->where('is_santa', 0);
    }

    /**
     * @param $query
     * @param $id
     * @return mixed
     */
    public function scopeNotPicked($query, $id)
    {
        return $query->where('has_been_picked', 0)->where('id', '<>', $id);
    }
}
