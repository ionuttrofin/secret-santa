<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\File\File;

class Importer
{
    const ROOT = '/import';

    /**
     * Importer constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $file
     * @return \PhpOffice\PhpSpreadsheet\Spreadsheet
     */
    public static function getExcel($file)
    {
        $excel = IOFactory::load($file->getPathName());
        return $excel;
    }

    /**
     * @param $excel
     * @param $sheet_name
     * @return mixed
     */
    public static function getSpreadSheet($excel, $sheet_name)
    {
        return $excel->getSheetByName($sheet_name);
    }


    /**
     * @param UploadedFile $originalFile
     * @return File
     */
    public static function fileToTemp(UploadedFile $originalFile)
    {
        $fName = Carbon::now('Europe/Bucharest')->format('Y-m-d H-i-s') . '-' . uniqid() . '-' . rand(1000, 100000);
        $importFile = $originalFile->move(storage_path() . self::ROOT, $fName . '.' . $originalFile->getExtension());

        return $importFile;
    }
}